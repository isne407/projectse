<?php 
    include_once "register.php";
?>

<!DOCTYPE html>
<html>
<head>
    	<script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>


    	<meta charset="UTF-8">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
        <link rel="stylesheet" href="styles/main.css" /> 


<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Document</title>
    <link rel="icon" type="image/png" href="dist/img/mail.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="vendor/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


</head>


<body class="hold-transition login-page" style="background-image:url(dist/img/book.jpg); height: 100%; background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
    <div class="row">
        <div class="col-md-6">
            <!--<img src="dist/img/login.jpg" style="height: 100%; background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">-->
        </div>
        <div class="col-md-4" style="top:190px;">
            <div class="login-box-body">
                <div class="login-box">
                    <div class="login-logo">
                        <span class="logo-lg">
                            <a href="login.html"><img src="dist/img/mail.png" style=" width:50px; height: auto; ">
                                <b>E-document</b></a>
                        </span>
                        <div style="font-size: 12px; padding-left: 12px;">ระบบ รับ-ส่ง หนังสือ</div>
                    </div>

<!-- /.login-logo -->
        <?php
            if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?>
        
    <form action="login.php" method="post">
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Username" name="username">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
                        
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
            
            <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="Sign In">
                </div>
                </form>
            </div>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 3 -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="vendor/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
    $(document).ready(function() {
        $('.sidebar-menu').tree()
    })
    </script>
</body>
 <!--

sing up form

<div id="flip" class="crecateBox"><b>Create an Account</b></div>
<div id="panel"><h2>Sign Up Form</h2>
	<?php
	if (!empty($error_msg)) {
            echo $error_msg;
        }
    ?>
    
<form method="post" action="<?php echo ($_SERVER['PHP_SELF']); ?>">

	<label><b>Username </b></label><br>
    <input type='text' 
    	   name='username' 
    	   required class="inputSingup" />
    	   <br><br>   

    <label><b>Email</b></label><br>
    <input type="text" 
    	   name="email" 
           required class="inputSingup"/>
    	   <br><br>

    <label><b>Firstname </b></label><br>
    <input type='text' 
    	   name='firstname' 
    	   required class="inputSingup" />
    	   <br><br>

    <label><b>Lastname </b></label><br>
    <input type='text' 
    	   name='lastname' 
    	   required class="inputSingup" />
    	   <br><br> 
             	
    <label><b>Password</b></label><br>
    <input type="password"
           name="password" 
           required class="inputSingup">
           <br><br>
    <input type="submit"
           name="submit"
		   value="Register"
	       class="button" 
           onclick="return regformhash(this.form,
                                        this.form.username,
                                        this.form.email,
                                   	    this.form.password,
                                        this.form.confirmpwd);" /> 	
                                       <ul>
            Usernames may contain only digits, upper and lower case letters and underscores<br>
            Emails must have a valid email format<br>
            Passwords must be at least 6 characters long<br>
            Passwords must contain <br>
                    At least one upper case letter (A..Z)<br>
                    At least one lower case letter (a..z)<br>
                    At least one number (0..9)<br>
                </ul>
            
            Your password and confirmation must match exactly<br><br>
        </ul>

</div>
</form>

    
    </body>
-->

</html>