<?php
     $id = $_POST['id'];
     $no_in = $_POST['no_in'];
     $topic = $_POST['topic'];
     $status = $_POST['statuses'];
     $username =$_POST['username'];
     $dates = $_POST['dates'];
     $note = $_POST['note'];
     $objective = $_POST['objective'];
     $book_to = $_POST['book_to'];
     $book_from = $_POST['book_from'];
     $staff = $_POST['staff'];
     $files = $_POST['files'];

?>

    <?php

    session_start();

    if(!$_SESSION['userid']){
        header("Location: index.php");
    } else{

?><!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Document</title>
    <link rel="icon" type="image/png" href="dist/img/mail.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="vendor/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <img class="logo-mini" style=" width:50px; height: auto; padding-top:1px;" src="dist/img/mail.png">
                <span class="logo-lg">
                    <img src="dist/img/mail.png" style=" width:50px; height: auto; padding-top:1px;">
                    E-document
                </span>
                <!-- logo for regular state and mobile devices -->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                    <?php echo $_SESSION['user']; ?>
                                        <small>ตำแหน่งงาน:</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="profile.html" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- จบ barnav-->
                </div>
            </nav>
        </header>
        <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- search form -->
                <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="ค้นหา...">
                        <span class="input-group-btn">
                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>
                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ไฟล์</li>
                <li>
                    <a href="document_page.php">
                        <i class="fa fa-folder-o"></i> <span>หนังสือเข้า</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">4</span>
                        </span>
                    </a>
                </li>
                <li class="header">
                    <i class="fa fa-files-o"></i>
                    <span>หนังสือรับ</span></a>
                </li>
                <li class="treeview">
                <li><a href="document_in.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกหนังสือรับ</span></a>
                </li>
                <li><a href="document_page.php">
                        <i class="fa fa-table"></i>
                        <span>เอกสารหนังสือรับ</span></a>
                </li>
                </li>
                <li class="header">
                    <i class="fa fa-files-o"></i>
                    <span>หนังสือส่ง</span>
                </li>
                <li class="treeview">
                <li><a href="document_in.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกหนังสือส่ง</span></a>
                </li>
                <li><a href="document_page.php">
                        <i class="fa fa-table"></i>
                        <span>เอกสารหนังสือส่ง</span></a>
                </li>
                </li>
                <li class="header">
                    <i class="fa  fa-file-text-o"></i>
                    <span>รายงานการประชุม</span>
                </li>
                <li class="treeview">
                <li><a href="document_in.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกรายงานการประชุม</span></a>
                </li>
                <li><a href="document_page.php">
                        <i class="fa fa-table"></i>
                        <span>บันทึกรายงานการประชุม</span></a>
                </li>
                </li>
            </ul>
            </section>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>หนังสือรับ
                    <small>เอกสารหนังสือรับ</small>
                    <small> &gt;</small>
                    <small>อ่านเอกสาร</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box box-primary">
                            <!-- Header-box-->
                            <div class="box-header with-border">
                                <h3 class="box-title">เอกสารหนังสือรับ</h3>
                                <form action="delete.php" method="post">
                                    <button type="submit" class="btn btn-danger pull-right">
                                        ลบ
                                    <i class="fa fa-edit"></i>
                                    <input name = "id" type="text" value="<?php echo $id ?>" hidden><br>
                                    </button>
                                </form>
                                <button type="submit" class="btn btn-warning pull-right">
                                    <i class="fa  fa-mail-forward"></i> ส่งต่อ</button>
                            </div>
                            <!-- /Header-box-->
                            <div class="box-body">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>ลงวันที่: <?php echo $dates ?></b>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>เลขที่รับ: <?php echo $no_in ?></b>
                                            </div>
                                            <div class="col-md-6">
                                                <b>เลขที่หนังสือ/ประเภท: </b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>หนังสือจาก: <?php echo $book_from ?> </b>
                                            </div>
                                            <div class="col-md-6">
                                                <b>หนังสือถึง: <?php echo $book_to ?> </b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>เรื่อง: <?php echo $topic ?></b>
                                            </div>
                                            <div class="col-md-6">
                                                <b>วัตถุประสงค์: <?php echo $objective ?></b>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <b>เจ้าหน้าที่: <?php echo $staff ?></b>
                                    </li>
                                    <li class="list-group-item">
                                        <b>หมายเหตุ:  <?php echo $note ?></b>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer"><a href="document_page.php">
                                <button type="submit" class="btn btn-default pull-right">กลับ</button>
                                </a>
                            </div>
                            <!-- /.box-footer -->
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            
                        </div>
                        <iframe src="files/<?php echo $files ?>" type=frame&vlink=xx&link=xx&css=xxx&bg=xx&bgcolor=xx
                                marginwidth=0 marginheight=0 hspace=0 vspace=0 frameborder=0 scorlling=yes width=540px height=720px></iframe>

                    </div>
                </div>
            </section>
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <strong>ภาควิชาวิศวกรรมคอมพิวเตอร์
                    <small>มหาวิทยาลัยเชียงใหม่</small>
                </strong>
            </div>
        </footer>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="vendor/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
    $(document).ready(function() {
        $('.sidebar-menu').tree()
    })
    </script>
</body>

</html>

<?php } ?>

