
 <?php

session_start();

if(!$_SESSION['userid']){
    header("Location: index.php");
} else{

?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>E-Document</title>
<link rel="icon" type="image/png" href="dist/img/mail.png">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="vendor/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="dist/css/dropify.min.css">

</head>

<body class="hold-transition skin-blue-light sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <img class="logo-mini" style=" width:50px; height: auto; padding-top:1px;" src="dist/img/mail.png">
            <span class="logo-lg">
                <img src="dist/img/mail.png" style=" width:50px; height: auto; padding-top:1px;">
                E-document
            </span>
            <!-- logo for regular state and mobile devices -->
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            </a>
            <div style="position: absolute;left: 50px;">
      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form" style="width: 600px;border: 0px;">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>
      <!-- /.search form -->
    </div>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                <p>
                                <?php echo $_SESSION['user']; ?>
                                    <small>ตำแหน่งงาน:</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="user_page.php" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- จบ barnav-->
            </div>
        </nav>
    </header>
    <!-- =============================================== -->
    <!-- Left side column. contains the sidebar -->
    <?php include_once 'sidemanu.php' ?>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">เอกสารหนังสือรับ</h3>
                            <small>จำนวนหนังสือทั้งหมด ฉบับ</small>
                            <div class="box-tools">
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                      
<?php
$mysqli = new mysqli("localhost", "root", "", "projectse");
$qry = "SELECT * FROM document_in order by id desc" ;
$result = $mysqli->query($qry);
?>
<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <tbody>
            <tr>
                <th>สถานะ</th>
                <th>เลขที่รับ</th>
                <th>เลขที่หนังสือ/ประเภท</th>
                <th>เรื่อง</th>
                <th>ผู้ส่ง</th>
                <th>ส่งวันที่</th>
                <th>วัตถุประสงค์</th>
            </tr>
            <?php 
                while ($row = $result->fetch_array()){
            ?>
            <tr><form action="read_doc.php" method="post">
                <td>
                <label>
                  <span class="label label-warning"><?php echo $row['statuses']; ?></span>
                  <input type="text" name="statuses" value="<?php echo $row['statuses']; ?>" hidden>
                </label>
                </td>
                <td><label>
                  <span><?php echo $row['id']; ?></span>
                  <input type="text" name="id" value="<?php echo $row['id']; ?>" hidden>
                </label></td>
                <td><label>
                  <span><?php echo $row['no_in']; ?></span>
                  <input type="text" name="no_in" value="<?php echo $row['no_in']; ?>" hidden>
                </label></td>
                <td><label>
                  <span><?php echo $row['topic']; ?></span>
                  <input type="text" name="topic" value="<?php echo $row['topic']; ?>" hidden>
                </label></td>
                <td><label>
                  <span><?php echo $row['username']; ?></span>
                  <input type="text" name="username" value="<?php echo $row['username']; ?>" hidden>
                </label></td>
                <td><label>
                  <span><?php echo $row['dates']; ?></span>
                  <input type="text" name="dates" value="<?php echo $row['dates']; ?>" hidden>

                  <input type="text" name="note" value="<?php echo $row['note']; ?>" hidden>
                  <input type="text" name="objective" value="<?php echo $row['objective']; ?>" hidden>
                  <input type="text" name="book_to" value="<?php echo $row['book_to']; ?>" hidden>
                  <input type="text" name="book_from" value="<?php echo $row['book_from']; ?>" hidden>
                  <input type="text" name="staff" value="<?php echo $row['staff']; ?>" hidden>
                  <input type="text" name="files" value="<?php echo $row['files']; ?>" hidden>


                  
                </label></td>
                <td><button class="button1" type="sybmit" name="submit">
                <style>
                .button1{
                  background-color: #56a4ef; border: none; color: white; padding: 5px; text-align: center; text-decoration: none; 
                  display: inline-block; margin: 5px; font-size: 16px; cursor: pointer; border-radius: 8px;
                }
                </style>
                ดูเอกสาร</button></td>
                </form>
            </tr>
            <?php 
                }
            ?>                  
        </tbody>
    </table>
</div>
</div>
                            <!-- /.box-footer -->
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <strong>ภาควิชาวิศวกรรมคอมพิวเตอร์
                <small>มหาวิทยาลัยเชียงใหม่</small>
            </strong>
        </div>
    </footer>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="vendor/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="vendor/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script>
$(document).ready(function() {
    $('.sidebar-menu').tree()
})
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="dist/js/dropify.min.js"></script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            
            });

           
    </script>
</body>

</html>

<?php } ?>

