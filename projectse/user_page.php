<?php

    session_start();

    if(!$_SESSION['userid']){
        header("Location: index.php");
    } else{

?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Document</title>
    <link rel="icon" type="image/png" href="dist/img/mail.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="vendor/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="document_page.php" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <img class="logo-mini" style=" width:50px; height: auto; padding-top:1px;" src="dist/img/mail.png">
                <span class="logo-lg">
                    <img src="dist/img/mail.png" style=" width:50px; height: auto; padding-top:1px;">
                    E-document
                </span>
                <!-- logo for regular state and mobile devices -->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                    <?php echo $_SESSION['user']; ?>
                                        <small>ตำแหน่งงาน:</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="user_page." class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- จบ barnav-->
                </div>
            </nav>
        </header>
        <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->
       <?php include_once 'sidemanu.php' ?>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>ประวัติส่วนตัว
                <small>แก้ไขประวัติส่วนตัว</small>
                </h1>
                 <ol class="breadcrumb">
                   <li><a href="#"><i class="fa fa-gears"></i></a></li> 
                 </ol>  
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-md-3">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="dist/img/boy.png" alt="User profile picture">
                                <h3 class="profile-username text-center">ชื่อผู้ใช้</h3>
                                <p><center><h4><?php echo $_SESSION['username']; ?></h4></center></p>
                                <p class="text-muted text-center">ตำแหน่ง</p>
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                        <b>E-mail : </b> <?php echo $_SESSION['useremail']; ?>
                                    </li>
                                    <li class="list-group-item">
                                        <b>ชื่อ-นามสกุล :</b> <?php echo $_SESSION['user']; ?>
                                    </li>
                                    <li class="list-group-item">
                                        <b>เบอร์โทร</b>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box --->
                        <!-- About Me Box -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">เกี่ยวกับฉัน</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <strong><i class="fa fa-map-marker margin-r-5"></i>ที่อยู่/ตำแน่งงาน</strong>
                                <p class="text-muted">Malibu, California</p>
                                <hr>
                                <strong><i class="fa fa-file-text-o margin-r-5"></i>บันทึก</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">แก้ไขประวัติส่วนตัว</h3>
                            </div>
                                <div class="box-body">
                                    <form class="form-horizontal" action="user_update.php" method="post">
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-2 control-label">ชื่อ</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputName" placeholder="Name" name="name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputName" class="col-sm-2 control-label">นามสกุล</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="password" placeholder="Last Name" name="lastname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-sm-2 control-label">ชื่อเข้าใช้งาน</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputEmail" placeholder="Username" name="username">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" id="inputEmail" placeholder="E-mail" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-2 control-label">ตำแหน่ง</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputSkills" placeholder="ตำแหน่ง" name="position">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputSkills" class="col-sm-2 control-label">เบอร์โทร</label>
                                            <div class="col-sm-10">
                                                <input type="int" class="form-control" id="inputSkills" placeholder="เบอร์โทร" name="phone_no">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputExperience" class="col-sm-2 control-label">บันทึก</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="inputExperience" placeholder="บันทึก" name="note"></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <input type="text" name="usertest" value="<?php echo $_SESSION['username']; ?>" hidden>
                                                <button type="submit" class="btn btn-danger" name="submit">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>E-ducoment ISNE</strong>
        </footer>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="vendor/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
    $(document).ready(function() {
        $('.sidebar-menu').tree()
    })
    </script>
</body>

</html>

<?php } ?>







