
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        table,th,td{
            border :1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<?php
 $mysqli = new mysqli("localhost", "root", "", "projectse");
 $qry = "SELECT * FROM document_in";
 $result = $mysqli->query($qry);
?>
<table style="width:500px">
       <tr>
            <th>สถานะ
            <th>เลขที่รับ</th>
            <th>เลขที่หนังสือ/ประเภท</th>
            <th>เรื่อง</th>
            <th>ผู้ส่ง</th>
            <th>ส่งวันที่</th>
            <th>วัตถุประสงค์</th>
        </tr>
       <?php 
        while ($row = $result->fetch_array()){
       ?>
        <tr>
            <td><?php echo $row['id']; ?></td>
            <td><?php echo $row['staff']; ?></td>
            <td><?php echo $row['no_in']; ?></td>
            <td><?php echo $row['note']; ?></td>
            <td><?php echo $row['topic']; ?></td>
            <td><?php echo $row['dates']; ?></td>
            <td><a href="readdoc.html">เพื่อทราบ</a></td>
        </tr>
        <?php 
       }
        ?>
</table>
</body>
</html>
