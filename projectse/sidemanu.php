<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- search form -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="ค้นหา...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </form>
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ไฟล์</li>
                <li>
                    <a href="document_page.php">
                        <i class="fa fa-folder-o"></i> <span>หนังสือเข้า</span>
                        <span class="pull-right-container">
                            <span class="label label-primary pull-right">4</span>
                        </span>
                    </a>
                </li>
                <li class="header">
                    <i class="fa fa-files-o"></i>
                    <span>หนังสือรับ</span></a>
                </li>
                <li class="treeview">
                <li><a href="document_in.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกหนังสือรับ</span></a>
                </li>
                <li><a href="document_in_read.php">
                        <i class="fa fa-table"></i>
                        <span>เอกสารหนังสือรับ</span></a>
                </li>
                </li>
                <li class="header">
                    <i class="fa fa-files-o"></i>
                    <span>หนังสือส่ง</span>
                </li>
                <li class="treeview">
                <li><a href="document_out.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกหนังสือส่ง</span></a>
                </li>
                <li><a href="document_out_read.php">
                        <i class="fa fa-table"></i>
                        <span>เอกสารหนังสือส่ง</span></a>
                </li>
                </li>
                <li class="header">
                    <i class="fa  fa-file-text-o"></i>
                    <span>รายงานการประชุม</span>
                </li>
                <li class="treeview">
                <li><a href="document_in.php">
                        <i class="fa fa-pencil"></i>
                        <span>กรอกรายงานการประชุม</span></a>
                </li>
                <li><a href="document_page.php">
                        <i class="fa fa-table"></i>
                        <span>บันทึกรายงานการประชุม</span></a>
                </li>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>