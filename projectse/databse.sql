--database name projectse

-- user table
CREATE TABLE user (
    id INT(5) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    email VARCHAR(128) NOT NULL, 
    firstname VARCHAR(128) NOT NULL, 
    lastname VARCHAR(128) NOT NULL, 
    password VARCHAR(128) NOT NULL, 
    userlevel VARCHAR(1) NOT NULL,
    position text(64),
    phone text(32),
    note text(64)
)ENGINE = InnoDB DEFAULT CHARSET=utf8


--document_in 
CREATE TABLE document_in (
    id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    no_in TEXT(128), 
    book_from TEXT(128), 
    book_to TEXT(128), 
    topic TEXT(128), 
    statuses TEXT(128), 
    staff TEXT(128), 
    note TEXT(128), 
    username VARCHAR(50), 
    dates VARCHAR(50), 
    objective VARCHAR(50), 
    files VARCHAR(128)
)ENGINE = InnoDB DEFAULT CHARSET=utf8


--document_out
CREATE TABLE document_out (
    id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    no_in TEXT(128), 
    book_from TEXT(128), 
    book_to TEXT(128), 
    topic TEXT(128), 
    statuses TEXT(128), 
    staff TEXT(128), 
    note TEXT(128), 
    username VARCHAR(50), 
    dates VARCHAR(50), 
    objective VARCHAR(50), 
    files VARCHAR(128)
)ENGINE = InnoDB DEFAULT CHARSET=utf8


--report 
CREATE TABLE report (
    id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    no_in TEXT(128), 
    book_from TEXT(128), 
    book_to TEXT(128), 
    topic TEXT(128), 
    statuses TEXT(128), 
    staff TEXT(128), 
    note TEXT(128), 
    username VARCHAR(50), 
    dates VARCHAR(50), 
    objective VARCHAR(50), 
    files VARCHAR(128)
)ENGINE = InnoDB DEFAULT CHARSET=utf8