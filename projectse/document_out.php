<?php

    session_start();

    if(!$_SESSION['userid']){
        header("Location: index.php");
    } else{

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Document</title>
    <link rel="icon" type="image/png" href="dist/img/mail.png">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="vendor/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="dist/css/dropify.min.css">
</head>

<body class="hold-transition skin-blue-light sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="#" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <img class="logo-mini" style=" width:50px; height: auto; padding-top:1px;" src="dist/img/mail.png">
                <span class="logo-lg">
                    <img src="dist/img/mail.png" style=" width:50px; height: auto; padding-top:1px;">
                    E-document
                </span>
                <!-- logo for regular state and mobile devices -->
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                    <?php echo $_SESSION['user']; ?>                                        <small>ตำแหน่งงาน:</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="profile.html" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- จบ barnav-->
                </div>
            </nav>
        </header>
        <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->
        <?php include_once 'sidemanu.php' ?>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>หนังสือส่ง
                    <small>กรอกหนังสือส่ง</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <div class="box box-primary">
                    <!-- Header-box-->
                    <div class="box-header with-border">
                        <h3 class="box-title">กรอกหนังสือส่ง</h3>
                    </div>
                    <!-- /Header-box-->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="document_out_add.php" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">เลขที่รับ:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputnum" name="no_id">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">เลขที่หนังสือ/ประเภท:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputtype" name="no_book_type">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">หนังสือจาก:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputform" name="book_from">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">หนังสือถึง:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputto" name="book_to">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">เรื่อง:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputtile" name="topic">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">วัตถุประสงค์:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputfor" name="objective">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">สถานะ:</label>
                                                <div class="col-md-6">
                                                    <select class="form-control" id="inputstatus" name="statuses">
                                                        <option>กรุณาเลือก</option>
                                                        <option>ไม่มี</option>
                                                        <option>ยกเลิก</option>
                                                        <option>ด่วนที่สุด</option>
                                                        <option>ด่วนมาก</option>
                                                        <option>ด่วน</option>
                                                        <option>ลับ</option>
                                                        <option>ลับมาก</option>
                                                        <option>ลับที่สุด</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">เจ้าหน้าที่:</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="inputuser" name="staff">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="inputnum" class="col-md-4 control-label">หมายเหตุ:</label>
                                                <div class="col-md-6">
                                                    <textarea class="form-control" rows="3" spellcheck="false" id="inputadd" name="note"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <!-- files upload -->
                                    <input type="file" id="input-file-now-custom-2" class="dropify" data-height="485" name="FileToUpload"/>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <input type="text" name="usertest" value="<?php echo $_SESSION['user']; ?>" hidden>
                                <input type="text" name="dates" value="<?php echo date('D d F Y')  ?>" hidden>
                                <button type="submit" class="btn btn-default">Cancel</button>
                                <button type="submit" class="btn btn-info pull-right" name="submit">submit</button>
                            </div>
                            <!-- /.box-footer -->
                    </form>
                </div>
        </div>
    </div>
    </section>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <strong>ภาควิชาวิศวกรรมคอมพิวเตอร์
                <small>มหาวิทยาลัยเชียงใหม่</small>
            </strong>
        </div>
    </footer>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    <script src="vendor/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="vendor/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
    $(document).ready(function() {
        $('.sidebar-menu').tree()
    })
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();


    });
    </script>
</body>

</html>

<?php } ?>